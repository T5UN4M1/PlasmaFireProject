#pragma once

#include "Galaxy.h"
#include "GWindow.h"

#include <list>

enum GameState {
	SYSTEM_VIEW,
	GALAXY_VIEW
};
class Game
{
protected:
	GameState state;
	Galaxy galaxy;

	sf::Vector2f cameraCenter;
	sf::Vector2f screenSize;
	unsigned zoomLevel;


	int currentPlayerId; // l'ID du joueur qui joue la partie localement ( ID 0 = non affili� � un joueur)

	std::list<GWindow*> gWindows;
	int mouseLeftClickValue;
	int frame;
public:
	Game();
	void yield();
	void createBasicGame();

	void drawEntity(Entity entity, SingleSprite sp);
	bool isClicked(Entity entity, Selectable selectable);

};


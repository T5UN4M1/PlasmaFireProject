#include "Core.h"

#include "Media.h"
#include "LoadScreen.h"
#include "MenuScreen.h"
#include "Game.h"

std::unique_ptr<sf::RenderWindow> Core::window;

sf::RenderTexture Core::mainPanel;
sf::Sprite Core::spMainPanel;

sf::Vector2f Core::cursorPos;
bool Core::cursorVisible;

int Core::frame = 0;
CoreState Core::state = CoreState::STARTED;

std::unique_ptr<Game> Core::game;
void Core::run()
{
	init();

	while (window->isOpen()) {
		sf::Event event;
		while (window->pollEvent(event)) {
			if (event.type == sf::Event::Closed || sf::Keyboard::isKeyPressed(sf::Keyboard::Escape)) {
				window->close();
			}
		}

		window->clear();

		yield();

		window->display();
	}
}

void Core::yield()
{

	switch (state) {
	case STARTED:
		if (frame == 0) {
			Media::load(Media::loadMinimal);
		}
		if (!Media::loading) {
			setState(CoreState::LOADING_FOR_MENU);
		}
		break;
	case LOADING_FOR_MENU:
		if (frame == 0) {
			Media::load(Media::loadMenu);
		}
		if (!Media::loading) {
			setState(CoreState::MENU);
		}
		LoadScreen::yield();
		buildMainPanel();
		drawMainPanel();
		break;
	case MENU:
		MenuScreen::yield();
		buildMainPanel();
		drawMainPanel();
		break;
	case LOADING_FOR_GAME:
		if (frame == 0) {
			Media::load(Media::loadGame);
		}
		if (!Media::loading) {
			setState(CoreState::GAME);
		}
		LoadScreen::yield();
		buildMainPanel();
		drawMainPanel();
		break;
	case GAME:
		if (frame == 0) {
			game = std::make_unique<Game>();
			game->createBasicGame();
		}
		game->yield();
		break;
	}
	++frame;

}

void Core::init()
{
	// setting up the window
	window = std::make_unique<sf::RenderWindow>(sf::VideoMode::getDesktopMode(), "PlasmaFireProject", sf::Style::Fullscreen);
	window->setVerticalSyncEnabled(true);
	window->setMouseCursorVisible(true);
	window->setMouseCursorGrabbed(true);
	mainPanel.create(1920, 1080);
}
void Core::setState(CoreState newState)
{
	state = newState;
	frame = 0;
	yield();
}
void Core::buildMainPanel() {
	mainPanel.display();
	spMainPanel.setTexture(mainPanel.getTexture());
}
void Core::drawMainPanel() {
	window->draw(spMainPanel);
	mainPanel.clear();
}
#include "LoadScreen.h"

#include "Core.h"
#include "Media.h"
#include "Tools.h"

int LoadScreen::progression = 0;
int LoadScreen::frame = 0;
sf::VertexArray LoadScreen::loadingBar(sf::Quads, 4 * 3);

void LoadScreen::resetProgression()
{
	progression = 0;
	frame = 0;
	updateLoadingBar();
}

void LoadScreen::addProgression(int progression)
{
	LoadScreen::progression += progression;
	if (LoadScreen::progression > MAX_PROGRESSION) {
		LoadScreen::progression = MAX_PROGRESSION;
	}
	updateLoadingBar();
}

float LoadScreen::getProgressionPercent()
{
	return (static_cast<float>(progression) / static_cast<float>(MAX_PROGRESSION)) * 100.0;
}

void LoadScreen::yield()
{
	//Core::mainPanel.draw(Media::loadScreenBg);

	sf::Transform t;
	t.translate(50, 900); // positioning loading bar
	Core::mainPanel.draw(loadingBar, t);

}

void LoadScreen::updateLoadingBar()
{
	const int barWidth = 1000;
	const int barHeight = 7;
	const int barBorderThickness = 1;

	// bar border part (only border will remain thanks to background
	makeRectangle(loadingBar,
		0,
		sf::FloatRect(0, 0, barWidth, barHeight),
		sf::Color::White);

	// bar background part (to make a large "hole" into the border rectangle to form the bar
	makeRectangle(loadingBar,
		1,
		sf::FloatRect(barBorderThickness, barBorderThickness, barWidth - barBorderThickness * 2, barHeight - barBorderThickness * 2),
		sf::Color::Black);

	// loading bar part
	makeRectangle(loadingBar,
		2,
		sf::FloatRect(barBorderThickness, barBorderThickness, (barWidth - barBorderThickness * 2) * (getProgressionPercent() / 100.0), barHeight - barBorderThickness * 2),
		sf::Color::Red);
}

#pragma once

#include <SFML/Graphics.hpp>
#include "Game.h"
#include <memory>


enum CoreState {
	STARTED,
	LOADING_FOR_MENU,
	MENU,
	LOADING_FOR_GAME,
	GAME
};

class Core
{
public:
	static std::unique_ptr<sf::RenderWindow> window;

	static std::unique_ptr<Game> game;

	static sf::RenderTexture mainPanel;
	static sf::Sprite spMainPanel;

	static int frame;  // frame counter 
	static CoreState state;

	static sf::Vector2f cursorPos;
	static bool cursorVisible;

	static void run(); // called at the start of the program
	static void yield(); // called in a loop from run() , main decider that calls the correct class according to the current state
	static void init(); // called at the start

	static void buildMainPanel();
	static void drawMainPanel();

	static void setState(CoreState newState);
};


#pragma once

#include <SFML/graphics.hpp>
#define MAX_PROGRESSION 1000000

class LoadScreen
{
public:
	static sf::VertexArray loadingBar;
	static int frame;

	static void resetProgression(); // progression goes back to 0
	static void addProgression(int progression); // adds to the progression
	static float getProgressionPercent(); // gets a percentage of current progression

	static void yield(); // draws loading screen to main rt
	static void updateLoadingBar();

private:
	static int progression;
};

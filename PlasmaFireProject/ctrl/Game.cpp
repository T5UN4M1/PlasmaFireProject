#include "Game.h"
#include "Core.h"
#include "Media.h"
#include "Tools.h"
#include "../Goods.h"
#include "GStation.h"

Game::Game()
{
	mouseLeftClickValue = 0;
}



void Game::yield()
{
	//Media::spSun.setPosition(500, 500);
	Core::mainPanel.draw(Media::spSpaceBackground);
	//Core::mainPanel.draw(Media::spSun);
	System& currentSystem = galaxy.systems[0];

	for (Planet& p : currentSystem.planets) {
		if (p.discoveredByPlayer.discovered[currentPlayerId]) {
			drawEntity(p.entity, p.singleSprite);
		}
		
	}

	for (Station& s : currentSystem.stations) {
		if (s.discoveredByPlayer.discovered[currentPlayerId]) {
			drawEntity(s.entity, s.singleSprite);
			if (isClicked(s.entity, s.selectable)) {
				gWindows.push_back(new GStation(sf::IntRect(50, 50, 300, 300), sf::Color::White, sf::Color::Blue,&s));
			}
		}
	}
	for (auto it = gWindows.begin(); it != gWindows.end(); ++it) {
		(*it)->generateWindowBackground(sf::Color::White,sf::Color::Blue);
		(*it)->updateSprite();
		(*it)->spWindow.setPosition((*it)->w.left, (*it)->w.top);
		Core::mainPanel.draw((*it)->spWindow);
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up)) {
		cameraCenter.y -=10;
	} else if(sf::Keyboard::isKeyPressed(sf::Keyboard::Down)) {
		cameraCenter.y += 10;
	} else if(sf::Keyboard::isKeyPressed(sf::Keyboard::Left)) {
		cameraCenter.x -= 10;
	} else if(sf::Keyboard::isKeyPressed(sf::Keyboard::Right)) {
		cameraCenter.x += 10;
	}


	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Add)) {
		zoomLevel -= 10;
	}
	else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Subtract)) {
		zoomLevel += 10;
	}

	if (sf::Mouse::isButtonPressed(sf::Mouse::Left)) {
		++mouseLeftClickValue;
	}
	else {
		mouseLeftClickValue = 0;
	}
	Core::buildMainPanel();
	Core::drawMainPanel();
	++frame;
}

void Game::createBasicGame()
{
	state = GameState::SYSTEM_VIEW;
	currentPlayerId = 1;
	
	zoomLevel = 1000;
	screenSize = sf::Vector2f(1920, 1080);
	cameraCenter = sf::Vector2f(screenSize.x / 2, screenSize.y / 2);
	galaxy = Galaxy();
	galaxy.addSystem(System());

	galaxy.systems[0].addPlanet(Planet());
	galaxy.systems[0].addPlanet(Planet());
	galaxy.systems[0].addStation(Station());

	Planet& p = galaxy.systems[0].planets[0];
	p.singleSprite.sprite = sf::Sprite(Media::spPlanet);
	p.entity.pos.x = 500;
	p.entity.pos.y = 500;
	p.entity.angle = 0;
	p.discoveredByPlayer.autoGen(4, false);
	p.discoveredByPlayer.discover(currentPlayerId);
	p.ownedByPlayer.playerId = currentPlayerId;

	Planet& p2 = galaxy.systems[0].planets[1]; /// � changer , on cr�e une planete qui est en fait un soleil ...
	p2.singleSprite.sprite = sf::Sprite(Media::spSun);
	p2.entity.pos.x = 800;
	p2.entity.pos.y = 800;
	p2.entity.angle = 0;
	p2.discoveredByPlayer.autoGen(4, false);
	p2.discoveredByPlayer.discover(currentPlayerId);

	Station& st = galaxy.systems[0].stations[0];
	st.singleSprite.sprite = sf::Sprite(Media::spStationTmp);
	st.entity.pos.x = 600;
	st.entity.pos.y = 600;
	st.entity.angle = 0;
	st.discoveredByPlayer.autoGen(4, false);
	st.discoveredByPlayer.discover(currentPlayerId);
	st.ownedByPlayer.playerId = currentPlayerId;
	st.selectable.clickHitbox = sf::Vector2i(100, 100);

	st.ressources.ressources[Goods::getGoodId("IRON_ORE")] = 500;
	st.ressources.ressources[Goods::getGoodId("COPPER_ORE")] = 500;
}

void Game::drawEntity(Entity entity, SingleSprite sp)
{
	//sp.sprite.setPosition((entity.pos.x + (cameraCenter.x - screenSize.x/2)) * (1000.0/zoomLevel),( entity.pos.y + (cameraCenter.y - screenSize.y/2)) * (1000.0 / zoomLevel)); // needs fixing to position when zooming
	sp.sprite.setPosition(screenSize.x/2 + (entity.pos.x - cameraCenter.x) * (1000.0 / zoomLevel), screenSize.y/2 + (entity.pos.y - cameraCenter.y) * (1000.0 / zoomLevel));
	sp.sprite.setScale((1000.0 / zoomLevel), (1000.0 / zoomLevel));
	sp.sprite.setRotation(entity.angle);
	Core::mainPanel.draw(sp.sprite);
}

bool Game::isClicked(Entity entity, Selectable selectable)
{
	return (sf::Mouse::isButtonPressed(sf::Mouse::Left) && mouseLeftClickValue == 1 && isPointInRectangle(sf::Mouse::getPosition(), centerDimensionToIntRect(sf::Vector2i(entity.pos.x, entity.pos.y), selectable.clickHitbox)));
}

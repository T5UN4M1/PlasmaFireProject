#include "MenuScreen.h"
#include "Media.h"
#include <SFML/Graphics.hpp>
#include "Core.h"
sf::Sprite MenuScreen::menuBg;

void MenuScreen::yield()
{
	sf::Text msg;
	msg.setFont(Media::fonts[0]);
	msg.setString("Press ENTER to start game");
	msg.setPosition(500, 700);
	msg.setFillColor(sf::Color::White);
	Core::mainPanel.draw(msg);

	msg.setPosition(500, 750);
	msg.setString("Press ESCAPE to quit");
	Core::mainPanel.draw(msg);

	msg.setPosition(500, 800);
	msg.setString("This game is in developpement, many features are expected to not work properly.");
	msg.setFillColor(sf::Color::Red);
	Core::mainPanel.draw(msg);

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Return)) {
		Core::setState(CoreState::LOADING_FOR_GAME);
	}
}

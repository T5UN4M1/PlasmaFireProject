#include "Media.h"
#include <thread>

#include "LoadScreen.h"
std::unordered_map<std::string, sf::Texture> Media::t; // all textures stored here

bool Media::loading = false; // is something loading right now ? (set to false at the end of each loading method)

std::vector<sf::Font> Media::fonts;

MediaLoadingState Media::state = NOTHING_LOADED;

#define ROOT_PATH "./"

#define ASSETS_PATH ROOT_PATH "assets/"

#define GUI_PATH ASSETS_PATH "gui/"
#define FONTS_PATH ASSETS_PATH "fonts/"

#include "../Goods.h"

sf::Sprite Media::spSpaceBackground;
sf::Sprite Media::spSun;
sf::Sprite Media::spPlanet;
sf::Sprite Media::spStationTmp;

void Media::loadMinimal() {
	loading = false;
}

void Media::loadMenu() {
	for (unsigned i = 0; i < 4; ++i) {
		fonts.push_back(sf::Font());
	}

	fonts[0].loadFromFile(FONTS_PATH"Arial.ttf");
	fonts[1].loadFromFile(FONTS_PATH"Gunship.ttf");
	fonts[2].loadFromFile(FONTS_PATH"OldLondon.ttf");
	fonts[3].loadFromFile(FONTS_PATH"SomethingStrange.ttf"); LoadScreen::addProgression(100000);

	loading = false;
}
void Media::loadGame() {
	t["spaceBackGround"].loadFromFile(ASSETS_PATH"game/spaceBackGround.png"); LoadScreen::addProgression(700000);
	t["sun"].loadFromFile(ASSETS_PATH"game/sun.png"); LoadScreen::addProgression(150000);
	t["planet"].loadFromFile(ASSETS_PATH"game/planet.png"); LoadScreen::addProgression(150000);
	t["stationTmp"].loadFromFile(ASSETS_PATH"game/stationTmp.png");

	spSpaceBackground.setTexture(t["spaceBackGround"]);

	spSun.setTexture(t["sun"]);
	spSun.setOrigin(100, 100);

	spPlanet.setTexture(t["planet"]);
	spPlanet.setOrigin(100, 100);

	spStationTmp.setTexture(t["stationTmp"]);
	spStationTmp.setOrigin(20, 20);

	Goods::loadGoods();

	loading = false;
}

void Media::load(void(*f)()) {
	LoadScreen::resetProgression();
	loading = true;

	std::thread t(f);
	t.detach();
}
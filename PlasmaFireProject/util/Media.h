#pragma once

#include <SFML/Graphics.hpp>

#include <string>
#include <unordered_map>

enum MediaLoadingState {
	NOTHING_LOADED,  // nothing has been loaded yet
	MINIMAL_LOADED,  // loadscreen assets loaded
	MENU_LOADED,     // menu assets loaded
	GAME_LOADED      // game assets loaded (no more loading required)
};

class Media
{
public:
	static std::unordered_map<std::string, sf::Texture> t; // all textures stored here

	static bool loading; // is something loading right now ? (set to false at the end of each loading method)

	static std::vector<sf::Font> fonts;

	static MediaLoadingState state;

	static sf::Sprite spSpaceBackground;
	static sf::Sprite spSun;
	static sf::Sprite spPlanet;
	static sf::Sprite spStationTmp;

	static void loadMinimal();
	static void loadMenu();
	static void loadGame();


	static void load(void(*function)());
};
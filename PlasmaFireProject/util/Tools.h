#pragma once

#include <SFML/graphics.hpp>

// rectangleId -> the rectangle number in the array (for example , rectangle with ID 3 will use vertices from 12 to 15)
void makeRectangle(sf::VertexArray& array, unsigned rectangleId, sf::FloatRect pos); // makes a rectangle in a vertexarray
void makeRectangle(sf::VertexArray& array, unsigned rectangleId, sf::FloatRect pos, sf::Color color); // same but also gives that rectangle a color

bool isPointInRectangle(sf::Vector2i point, sf::IntRect rectangle);
sf::IntRect centerDimensionToIntRect(sf::Vector2i point, sf::Vector2i dimensions);



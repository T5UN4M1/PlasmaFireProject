#include "Tools.h"

void makeRectangle(sf::VertexArray& array, unsigned rectangleId, sf::FloatRect pos)
{
	array[rectangleId * 4].position = sf::Vector2f(pos.left, pos.top);
	array[rectangleId * 4 + 1].position = sf::Vector2f(pos.left + pos.width, pos.top);
	array[rectangleId * 4 + 2].position = sf::Vector2f(pos.left + pos.width, pos.top + pos.height);
	array[rectangleId * 4 + 3].position = sf::Vector2f(pos.left, pos.top + pos.height);
}

void makeRectangle(sf::VertexArray & array, unsigned rectangleId, sf::FloatRect pos, sf::Color color)
{
	makeRectangle(array, rectangleId, pos);
	for (unsigned i = rectangleId * 4; i < (rectangleId + 1) * 4; ++i) {
		array[i].color = color;
	}
}

bool isPointInRectangle(sf::Vector2i point, sf::IntRect rectangle)
{
	return  !(point.x < rectangle.left || point.y < rectangle.top || point.x > rectangle.left+rectangle.width || point.y > rectangle.top + rectangle.height);
}

sf::IntRect centerDimensionToIntRect(sf::Vector2i point, sf::Vector2i dimensions)
{
	return sf::IntRect(point.x-dimensions.x/2,point.y-dimensions.y/2,dimensions.x,dimensions.y);
}


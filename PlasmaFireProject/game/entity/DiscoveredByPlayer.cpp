#include "DiscoveredByPlayer.h"

void DiscoveredByPlayer::autoGen(int amount, bool default)
{
	discovered.clear();
	discovered.resize(amount);

	for (int i = 0; i < amount; ++i) {
		discovered[i] = default;
	}
}

void DiscoveredByPlayer::discover(int playerId)
{
	discovered[playerId] = true; // causes crash for some reason
}

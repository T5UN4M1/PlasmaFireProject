#pragma once

#include <utility>
#include <vector>
#include <bitset>

class DiscoveredByPlayer
{
public:
	std::vector<int> discovered; // 0 = false , 1 = ture
	void autoGen(int amount, bool default);

	void discover(int playerId);
};


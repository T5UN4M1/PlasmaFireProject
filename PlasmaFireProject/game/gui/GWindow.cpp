#include "GWindow.h"
#include "Tools.h"
#include "Media.h"

GWindow::GWindow(sf::IntRect size, sf::Color borderColor,sf::Color  bgColor)
{
	w = size;
	windowBody = sf::VertexArray(sf::Quads, 2 * 4);
	generateWindowBackground(borderColor, bgColor);
	text.setFont(Media::fonts[0]);
}

void GWindow::generateWindowBackground(sf::Color borderColor, sf::Color bgColor)
{
	window.clear();
	makeRectangle(windowBody, 0,static_cast<sf::FloatRect>(w),borderColor);
	sf::IntRect bg = w;
	bg.left++;
	bg.top++;
	bg.height -= 2;
	bg.width -= 2;
	makeRectangle(windowBody, 1, static_cast<sf::FloatRect>(bg),bgColor);
	window.create(w.width, w.height);
	window.draw(windowBody);
}

void GWindow::updateSprite()
{
	window.display();
	spWindow.setTexture(window.getTexture());
}

#pragma once
#include <SFML\Graphics.hpp>

class GWindow
{
public:
	GWindow(sf::IntRect size,sf::Color borderColor,sf::Color bgColor);

	sf::RenderTexture window;
	sf::Sprite spWindow;


	sf::Text text;

	bool hasCloseButton;
	sf::IntRect w;
	sf::VertexArray windowBody;

	void generateWindowBackground(sf::Color borderColor,sf::Color bgColor);
	void updateSprite();

	virtual void drawGui()=0;

};

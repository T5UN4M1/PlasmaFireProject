#pragma once 

#include "GWindow.h"
#include "Station.h"
class GStation : public GWindow
{
public:
	Station* station;
	GStation(sf::IntRect size, sf::Color borderColor, sf::Color bgColor,Station* station);


	virtual void drawGui();
};

#pragma once
#include "System.h"
#include <vector>

class Galaxy
{
protected:
	
public:
	Galaxy();
	std::vector<System> systems;
	void addSystem(System& system);
};


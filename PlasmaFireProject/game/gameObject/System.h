#pragma once

#include <vector>
#include "Planet.h"

#include "Station.h"

class System
{
public:
	int id;
	std::vector<Planet> planets;


	std::vector<Station> stations;

	System();
	void addPlanet(Planet& planet);
	void addStation(Station& station);


};


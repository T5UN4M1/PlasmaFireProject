#pragma once

#include "DiscoveredByPlayer.h"
#include "Entity.h"
#include "OwnedByPlayer.h"
#include "Selectable.h"
#include "SingleSprite.h"
#include "Ressources.h"

class Station
{
public:
	Station();

	DiscoveredByPlayer discoveredByPlayer;
	Entity entity;
	OwnedByPlayer ownedByPlayer;

	SingleSprite singleSprite;

	Selectable selectable;
	Ressources ressources;

};

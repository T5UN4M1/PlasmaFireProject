#pragma once

#include "DiscoveredByPlayer.h"
#include "Entity.h"
#include "SingleSprite.h"


class SpaceEntity
{
public:
	SpaceEntity();


	DiscoveredByPlayer discoveredByPlayer;
	Entity entity;
	SingleSprite singleSprite;
};

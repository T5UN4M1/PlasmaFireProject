#pragma once
#include <iostream>
#include <string>
#include <vector>

class Goods {
	/*IRON_ORE,
	TITANIUM_ORE,
	IRIDIUM_ORE,
	COPPER_ORE,
	COBALT_ORE,
	NIOBIUM_ORE,

	IRON,
	TITANIUM,
	IRIDIUM,
	COPPER,
	COBALT,
	NIOBIUM,

	OIL,
	WATER,
	HEAVY_WATER,
	HYDROGEN,
	OXYGEN,
	NEON,


	T1_HULL_PLATE,
	T2_HULL_PLATE,
	T3_HULL_PLATE,


	BASIC_ALLOY,
	METAL_ALLOY,
	ADVANCED_ALLOY,
	HIGH_TEMPERATURE_ALLOY*/
public :
	static void loadGoods() {
		goods.clear();
		goods.insert(goods.begin(), {
			std::make_pair("IRON_ORE","Minerai de fer"),
			std::make_pair("TITANIUM_ORE","Minerai de cuivre"),
			std::make_pair("IRIDIUM_ORE","Minerai"),
			std::make_pair("COPPER_ORE","Minerai"),
			std::make_pair("COBALT_ORE","Minerai"),
			std::make_pair("NIOBIUM_ORE","Minerai"),

			std::make_pair("IRON","Minerai"),
			std::make_pair("TITANIUM","Minerai"),
			std::make_pair("IRIDIUM","Minerai"),
			std::make_pair("COPPER","Minerai"),
			std::make_pair("COBALT","Minerai"),
			std::make_pair("NIOBIUM","Minerai"),

			std::make_pair("OIL","Minerai"),
			std::make_pair("WATER","Minerai"),
			std::make_pair("HEAVY_WATER","Minerai"),
			std::make_pair("HYDROGEN","Minerai"),
			std::make_pair("OXYGEN","Minerai"),
			std::make_pair("NEON","Minerai"),


			std::make_pair("T1_HULL_PLATE","Minerai"),
			std::make_pair("T2_HULL_PLATE","Minerai"),
			std::make_pair("T3_HULL_PLATE","Minerai"),


			std::make_pair("BASIC_ALLOY","Minerai"),
			std::make_pair("METAL_ALLOY","Minerai"),
			std::make_pair("ADVANCED_ALLOY","Minerai"),
			std::make_pair("HIGH_TEMPERATURE_ALLOY","Minerai")
		});
	}
	static int getGoodId(std::string str) {
		for (int i = 0; i < goods.size(); ++i) {
			if (str == goods[i].first) {
				return i;
			}
		}
		return -1;
	}
	static std::string getGoodNameFromId(int id) {
		if (id < goods.size()) {
			return goods[id].second;
		}
		return "null";
	}
	static std::vector<std::pair<std::string, std::string>> goods;
};
